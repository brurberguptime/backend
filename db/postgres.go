package db

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/lib/pq"
	"gitlab.com/brurberg/log/v2"
)

type DBConnect struct {
	DB_HOST     string
	DB_USER     string
	DB_PASSWORD string
	DB_NAME     string
}

type DB struct {
	Conn *sql.DB
}

func (dbc DBConnect) ConnectToDB() (*sql.DB, error) {
	dbinfo := ""
	if dbc.DB_PASSWORD == "" {
		dbinfo = fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_NAME)
	} else {
		dbinfo = fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_PASSWORD, dbc.DB_NAME)
	}

	db, err := sql.Open("postgres", dbinfo)
	if err != nil {
		return nil, err
	}

	return db, db.Ping()
}

type SiteUptime struct {
	Hostname string
	Up       int
	Down     int
}

func (db DB) GetTodaysUptimeEntries() (sites []SiteUptime, err error) {
	rows, err := db.Conn.Query("SELECT hostname, count(CASE WHEN isup THEN 1 END) as up, count(CASE WHEN NOT isup THEN 1 END) as down FROM up WHERE isup = true AND createdat > DATE_TRUNC('day', NOW()) AND createdat < DATE_TRUNC('day', NOW()) + interval '1 day' GROUP BY hostname;")
	if err != nil {
		return
	}
	for rows.Next() {
		var siteUptime SiteUptime
		if log.CheckError("Fetching Sites", rows.Scan(&siteUptime.Hostname, &siteUptime.Up, &siteUptime.Down), log.Warning) {
			continue
		}
		sites = append(sites, siteUptime)
	}
	return
}

func (db DB) GetAllSites() (sites []string, err error) {
	rows, err := db.Conn.Query("SELECT hostname FROM sites;")
	if err != nil {
		return
	}
	for rows.Next() {
		var site string
		if log.CheckError("Fetching Sites", rows.Scan(&site), log.Warning) {
			continue
		}
		sites = append(sites, site)
	}
	return
}

type SiteInfo struct {
	Hostname    string
	Up          int
	Down        int
	FirstEntrie time.Time
	LastEntrie  time.Time
}

func (db DB) GetSiteInfo(site string) (siteInfo SiteInfo) {
	db.Conn.QueryRow("SELECT count(CASE WHEN isup THEN 1 END) as up, count(CASE WHEN NOT isup THEN 1 END) as down FROM up WHERE hostname = $1;", site).Scan(&siteInfo.Up, &siteInfo.Down)
	db.Conn.QueryRow("SELECT createdat FROM up WHERE hostname = $1 ORDER BY createdat ASC LIMIT 1;", site).Scan(&siteInfo.FirstEntrie)
	db.Conn.QueryRow("SELECT createdat FROM up WHERE hostname = $1 ORDER BY createdat DESC LIMIT 1;", site).Scan(&siteInfo.LastEntrie)
	siteInfo.Hostname = site
	return
}

func (db DB) GetSiteStatus(site string) (isUp bool) {
	db.Conn.QueryRow("SELECT isup FROM up WHERE hostname = $1 ORDER BY createdat DESC LIMIT 1;", site).Scan(&isUp)
	return
}
