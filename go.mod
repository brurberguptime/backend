module gitlab.com/brurberguptime/backend

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.9.0
	gitlab.com/brurberg/log/v2 v2.0.4
	gitlab.com/brurbergauth/backend v0.7.4
	gitlab.com/brurberglogs/backend v0.2.2
)
