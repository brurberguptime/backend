package main

import (
	"crypto/sha256"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab.com/brurberg/log/v2"
	"gitlab.com/brurbergauth/backend/lib/jwt"
	req "gitlab.com/brurbergauth/backend/lib/req/gin"
	"gitlab.com/brurberglogs/backend/utils"
	libDB "gitlab.com/brurberguptime/backend/db"
)

func loadEnvVar() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}
}

type DB struct {
	DB *libDB.DB
}

func TraceMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		sum := sha256.Sum256([]byte(fmt.Sprint(*c.Request)))
		if c.Keys == nil {
			c.Keys = make(map[string]interface{})
		}
		c.Keys["trace"] = fmt.Sprintf("%x", sum)
		c.Next()
	}
}

func main() {
	loadEnvVar()
	log.LogHandler = log.Logger{
		Level: log.Clean,
		Out: log.ExternLogger{
			Endpoint: "https://log.brurberg.no/api/newentry",
			LogToPackage: utils.PackageConsts{
				Domain: "uptime.brurberg.no",
				Key:    "Super Secret Key",
			}.LogToPackage,
		},
	}

	dbCon, dbErr := libDB.DBConnect{DB_HOST: os.Getenv("POSTGRES_HOST"), DB_USER: os.Getenv("POSTGRES_USER"), DB_PASSWORD: os.Getenv("POSTGRES_PASSWORD"), DB_NAME: os.Getenv("POSTGRES_DB")}.ConnectToDB()
	if dbErr != nil {
		log.Fatal(dbErr)
	}
	db := DB{DB: &libDB.DB{Conn: dbCon}}

	r := gin.Default()
	api := r.Group("/api")

	auth := api.Group("/")
	auth.Use(AuthMiddleware())
	auth.GET("/todaysuptime", db.TodaysUptimeEndpoint)
	auth.GET("/alltimeuptime", db.AlltimeUptimeEndpoint)
	auth.GET("/currentstatus", db.CurrentStatusEndpoint)

	log.CheckError("Start server", r.Run(":3000"), log.Critical)
}

type SiteUptime struct {
	Hostname         string  `json:"hostname"`
	Up               int     `json:"up"`
	Down             int     `json:"down"`
	Total            int     `json:"total"`
	EstimatedTotal   int     `json:"estimated_total"`
	PercenageUp      float64 `json:"percentage_up"`
	PercenageDown    float64 `json:"percentage_down"`
	PercenageUnknown float64 `json:"percentage_unknown"`
}

func (db DB) TodaysUptimeEndpoint(c *gin.Context) {
	entries, err := db.DB.GetTodaysUptimeEntries()
	if log.CheckError("Todays Uptime", err, log.Error) {
		c.JSON(500, gin.H{"error": err.Error()})
		return
	}

	sitesUptime := make([]SiteUptime, len(entries))
	now := time.Now()
	for i, entrie := range entries {
		et := now.Hour()*60 + now.Minute()
		if float64(et-entrie.Up-entrie.Down)*100/float64(et) < 0 {
			et += 1
		}
		sitesUptime[i] = SiteUptime{
			Hostname:         entrie.Hostname,
			Up:               entrie.Up,
			Down:             entrie.Down,
			Total:            entrie.Up + entrie.Down,
			EstimatedTotal:   et,
			PercenageUp:      float64(entrie.Up) * 100 / float64(et),
			PercenageDown:    float64(entrie.Down) * 100 / float64(et),
			PercenageUnknown: float64(et-entrie.Up-entrie.Down) * 100 / float64(et),
		}
	}

	c.JSON(200, sitesUptime)
}

func (db DB) AlltimeUptimeEndpoint(c *gin.Context) {
	sites, err := db.DB.GetAllSites()
	if log.CheckError("Alltime Uptime", err, log.Error) {
		c.JSON(500, gin.H{"error": err.Error()})
		return
	}

	sitesUptime := make([]SiteUptime, len(sites))
	for i, site := range sites {
		entrie := db.DB.GetSiteInfo(site)
		et := (entrie.LastEntrie.Unix() - entrie.FirstEntrie.Unix()) / 60
		if float64(int(et)-entrie.Up-entrie.Down)*100/float64(et) < 0 {
			et += 1
		}
		sitesUptime[i] = SiteUptime{
			Hostname:         entrie.Hostname,
			Up:               entrie.Up,
			Down:             entrie.Down,
			Total:            entrie.Up + entrie.Down,
			EstimatedTotal:   int(et),
			PercenageUp:      float64(entrie.Up) * 100 / float64(et),
			PercenageDown:    float64(entrie.Down) * 100 / float64(et),
			PercenageUnknown: float64(int(et)-entrie.Up-entrie.Down) * 100 / float64(et),
		}
	}

	c.JSON(200, sitesUptime)
}

type Status struct {
	Hostname string `json:"hostname"`
	IsUp     bool   `json:"isup"`
}

func (db DB) CurrentStatusEndpoint(c *gin.Context) {
	sites, err := db.DB.GetAllSites()
	if log.CheckError("Current Status", err, log.Error) {
		c.JSON(500, gin.H{"error": err.Error()})
		return
	}

	status := make([]Status, len(sites))
	for i, site := range sites {
		isUp := db.DB.GetSiteStatus(site)
		status[i] = Status{
			Hostname: site,
			IsUp:     isUp,
		}
	}

	c.JSON(200, status)
}

func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		handleErr := func(status int) func(error) {
			return func(err error) {
				c.JSON(status, gin.H{"error": err.Error()})
				c.Abort()
			}
		}
		bearerToken, err := req.GetBearerToken(c.Request.Header.Get("authorization"))
		if log.CheckError("AuthMiddleware", err, log.Warning) {
			handleErr(http.StatusBadRequest)
			return
		}
		claims := jwt.GetClaims(bearerToken)
		if c.Keys == nil {
			c.Keys = make(map[string]interface{})
		}
		c.Keys["claims"] = claims
		if h, ok := claims["host"]; !ok || c.Request.Host != h {
			c.JSON(http.StatusForbidden, gin.H{"error": "Missmatch in hosts"})
			c.Abort()
			return
		}
		_, err = jwt.Verify(bearerToken, []byte(os.Getenv("JWT_SECRET")))
		if log.CheckError("AuthMiddleware", err, log.Warning) {
			handleErr(http.StatusForbidden)
			return
		}
		c.Next()
	}
}
